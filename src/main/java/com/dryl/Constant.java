package com.dryl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;

public class Constant {
    public static Scanner scanner = new Scanner(System.in);
    public static Logger logger = LogManager.getLogger(Constant.class);
    public static ResourceBundle bundle = ResourceBundle.getBundle("project");
    public static List<Object> list = new ArrayList<>();
}
