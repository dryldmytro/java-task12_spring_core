package com.dryl.spring.model.config;

import com.dryl.spring.model.beans.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;

@Configuration
@Import(MinorBeanConfiguration.class)
@PropertySource("project.properties")
public class MainBeanConfiguration {
    @Bean("ABC")
    public BeanA getA1(BeanB b, BeanC c){
        String str = b.getName()+c.getName();
        int i = b.getValue()+c.getValue();
        return new BeanA(str,i);
    }
    @Bean("ABD")
    public BeanA getA2(BeanB b, BeanD d){
        String str = b.getName()+d.getName();
        int i = b.getValue()+d.getValue();
        return new BeanA(str,i);
    }
    @Bean("ACD")
    public BeanA getA3(BeanC c, BeanD d){
        String str = c.getName()+d.getName();
        int i = c.getValue()+d.getValue();
        return new BeanA(str,i);
    }
    @Bean(name = "BeanB",initMethod = "init",destroyMethod = "destroy")
    @DependsOn(value = {"BeanD","BeanC"})
    public BeanB getB(){
        return new BeanB();
    }
    @Bean(name = "BeanC",initMethod = "init",destroyMethod = "destroy")
    public BeanC getC(){
        return new BeanC();
    }
    @Bean(name = "BeanD",initMethod = "init",destroyMethod = "destroy")
    public BeanD getD(){
        return new BeanD();
    }
    @Bean
    public BeanG getG(){
        return new BeanG();
    }
    @Bean
    public MyBeanPostProcess getPostProc(){
        return new MyBeanPostProcess();
    }
}
