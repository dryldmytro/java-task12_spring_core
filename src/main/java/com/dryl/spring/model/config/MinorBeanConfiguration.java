package com.dryl.spring.model.config;

import com.dryl.spring.model.beans.BeanA;
import com.dryl.spring.model.beans.BeanE;
import com.dryl.spring.model.beans.BeanF;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;

@Configuration
@PropertySource("project.properties")
public class MinorBeanConfiguration {
    @Bean
    public BeanE getE(){
        return new BeanE();
    }
    @Bean
    @Lazy
    public BeanF getF1(@Qualifier("ABC") BeanA a){
        return new BeanF(a.getName(),a.getValue());
    }
    @Bean
    @Lazy
    public BeanF getF2(@Qualifier("ABD") BeanA a){
        return new BeanF(a.getName(),a.getValue());
    }
    @Bean
    @Lazy
    public BeanF getF3(@Qualifier("ACD") BeanA a){
        return new BeanF(a.getName(),a.getValue());
    }
}
