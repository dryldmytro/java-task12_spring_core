package com.dryl.spring.model.beans;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import static com.dryl.Constant.*;
@Component
public class BeanA implements BeanValidator, InitializingBean, DisposableBean {
    String name = "A";
    int value = 1;

    public BeanA() {
        list.add(this);
    }

    public BeanA(String a,int b) {
        this.name = name+a ;
        this.value=value+b;
        list.add(this);
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate() throws Exception {
        if (name.isEmpty()){
            throw new Exception();
        }
        if (value<0){
            throw new Exception();
        }
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("destroy A");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("afterProperties A");
    }
}
