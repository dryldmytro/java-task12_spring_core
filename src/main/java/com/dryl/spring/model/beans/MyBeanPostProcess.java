package com.dryl.spring.model.beans;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class MyBeanPostProcess implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof BeanValidator){
            try {
                ((BeanValidator) bean).validate();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return bean;
    }
}
