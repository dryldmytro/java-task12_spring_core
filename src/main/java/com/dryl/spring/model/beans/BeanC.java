package com.dryl.spring.model.beans;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import static com.dryl.Constant.*;

@Component
@PropertySource("project.properties")
public class BeanC implements BeanValidator {
    @Value("${nameC}")
    String name;
    @Value("${valueC}")
    int value;

    public BeanC() {
        list.add(this);
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate() throws Exception {
        if (name.isEmpty()){
            throw new Exception();
        }
        if (value<0){
            throw new Exception();
        }
    }
    public void init(){
        System.out.println("init bean C");
    }
    public void destroy(){
        System.out.println("destroyC");
    }
}
