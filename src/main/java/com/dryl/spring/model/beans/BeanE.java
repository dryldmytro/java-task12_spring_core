package com.dryl.spring.model.beans;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class BeanE implements BeanValidator {
    String name = "E";
    int value = 5;

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate() throws Exception {
        if (name.isEmpty()){
            throw new Exception();
        }
        if (value<0){
            throw new Exception();
        }
    }
    @PostConstruct
    public void postcons(){
        System.out.println("Postconstruct E");
    }
    @PreDestroy
    public void predest(){
        System.out.println("PreDestroy E");
    }
}
