package com.dryl.spring.model.beans;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.GenericBeanDefinition;

import java.lang.reflect.Method;

public class BeanG implements BeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
//        BeanDefinition bd = configurableListableBeanFactory.getBeanDefinition(BeanB.class);
        BeanDefinition bd = configurableListableBeanFactory.getBeanDefinition("BeanB");
        bd.setInitMethodName("init2");
    }

}
