package com.dryl.spring.model.beans;

import org.springframework.stereotype.Component;
import static com.dryl.Constant.*;
@Component
public class BeanF implements BeanValidator {
    String name = "F";
    int value= 6;

    public BeanF() {
        list.add(this);
    }

    public BeanF(String str, int i) {
        this.name = name+str;
        this.value = value+i;
        list.add(this);
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanF{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate() throws Exception {
        if (name.isEmpty()){
            throw new Exception();
        }
        if (value<0){
            throw new Exception();
        }
    }
}
