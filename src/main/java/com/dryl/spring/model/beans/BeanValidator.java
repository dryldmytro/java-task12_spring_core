package com.dryl.spring.model.beans;

public interface BeanValidator {
    void validate() throws Exception;
}
