package com.dryl.spring.model.beans;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import static com.dryl.Constant.list;

@Component
@PropertySource("project.properties")
public class BeanB implements BeanValidator {
    @Value("${nameB}")
    String name;
    @Value("${valueB}")
    int value;


    public BeanB() {
        list.add(this);
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate() throws Exception {
        if (name.isEmpty()){
            throw new Exception();
        }
        if (value<0){
            throw new Exception();
        }
    }
    public void init(){
        System.out.println("initB");
    }
    public void init2(){
        System.out.println("changed init method in bean B");
    }
    public void destroy(){
        System.out.println("destroyB");
    }
}
