package com.dryl.spring.controller;

import com.dryl.spring.model.beans.BeanA;
import com.dryl.spring.model.beans.BeanD;
import com.dryl.spring.model.beans.BeanE;
import com.dryl.spring.model.beans.BeanF;
import com.dryl.spring.model.config.MainBeanConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;

import static com.dryl.Constant.*;
public class Controller {
    private AnnotationConfigApplicationContext context;
    public Controller() {
        context = new AnnotationConfigApplicationContext(MainBeanConfiguration.class);
    }
    public void getBeanABasedOnBC(){
        BeanA a = context.getBean("ABC",BeanA.class);
        System.out.println(a);
    }
    public void getBeanABasedOnBD(){
        BeanA a = context.getBean("ABD",BeanA.class);
        System.out.println(a);
    }
    public void getBeanABasedOnCD(){
        BeanA a = context.getBean("ACD",BeanA.class);
        System.out.println(a);
    }
    public void showOrderOfCreation(){
        list.forEach(System.out::println);
    }
    public void getBeanFBasedOnABC(){
        BeanF f = context.getBean("getF1",BeanF.class);
        System.out.println(f);
    }
    public void getBeanFBasedOnABD(){
        BeanF f = context.getBean("getF3",BeanF.class);
        System.out.println(f);
    }
    public void getBeanFBasedOnACD(){
        BeanF f = context.getBean("getF2",BeanF.class);
        System.out.println(f);
    }
    public void getBeanE(){
        BeanE e = context.getBean(BeanE.class);
        System.out.println(e);
        context.close();
    }
    public void showAllBean(){
        for (String s:context.getBeanDefinitionNames()){
            logger.error(s);
        }
    }
    public void showAllConf(){
        for (String s:context.getBeanDefinitionNames()){
            System.out.println(context.getBeanDefinition(s));
        }
    }
}
