package com.dryl.spring;

import com.dryl.spring.view.View;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        View view = new View();
        view.outputMenu();
    }
}
