package com.dryl.spring.view;

import com.dryl.spring.controller.Controller;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.dryl.Constant.*;

public class View {

    private Controller controller;
    Map<String, String> map;

    public View() {
        controller = new Controller();
        map = new LinkedHashMap<>();
        map.put("1", "1.Show ABC");
        map.put("2", "2.Show ABD");
        map.put("3", "3.Show ACD");
        map.put("4", "4. Show Order Of Creation");
        map.put("5", "5.Show FABC");
        map.put("6", "6.Show FABD");
        map.put("7", "7.Show FACD");
        map.put("8", "8. Get bean E");
        map.put("9", "9.Show all beans.");
        map.put("10", "10.Show all beans config.");
        map.put("Q", "Q");
    }

    public void outputMenu() throws IOException {
        System.out.println("\nMENU:");
        for (String str : map.values()) {
            logger.info(str);
        }
        logger.trace(bundle.getString("choice"));
        String userChoice = scanner.nextLine().toUpperCase();
        switch (userChoice) {
            case "1":
                controller.getBeanABasedOnBC();
                outputMenu();
            case "2":
                controller.getBeanABasedOnBD();
                outputMenu();
            case "3":
                controller.getBeanABasedOnCD();
                outputMenu();
            case "4":
                controller.showOrderOfCreation();
                outputMenu();
                break;
            case "5":
                controller.getBeanFBasedOnABC();
                outputMenu();
                break;
            case "6":
                controller.getBeanFBasedOnABD();
                outputMenu();
                break;
            case "7":
                controller.getBeanFBasedOnACD();
                outputMenu();
                break;
            case "8":
                controller.getBeanE();
                outputMenu();
                break;
            case "9":
                controller.showAllBean();
                outputMenu();
                break;
            case "10":
                controller.showAllConf();
                outputMenu();
                break;
            case "Q":
                System.exit(0);
            default:
                logger.trace(bundle.getString("bad_choice"));
                outputMenu();
        }
    }
}
